echo on
set folder=%1
echo %folder%

if %folder%=="" goto compile

md %folder%\files1\mp3\worships\%folder%
md %folder%\files1\video\worships\%folder%

move %folder%\MP3\%folder%.mp3 %folder%\files1\mp3\worships\%folder%.mp3
move %folder%\MP3\*.mp3 %folder%\files1\mp3\worships\%folder%

move %folder%\MP4\%folder%.mp4 %folder%\files1\video\worships\%folder%.mp4
move %folder%\MP4\*.mp4 %folder%\files1\video\worships\%folder%
move %folder%\MP4\*.jpeg %folder%\files1\video\worships\%folder%
move %folder%\MP4\*.jpg %folder%\files1\video\worships\%folder%

rem goto compile

md E:\worships\%folder%
md E:\worships\%folder%\mp4

copy %folder%\MP4-HD\*.jpeg E:\worships\%folder%
move %folder%\MP4-HD\*.mp4 E:\worships\%folder%\mp4

:compile