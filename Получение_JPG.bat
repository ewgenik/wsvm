echo on

set arg1=%1
set foldernum=%arg1:~-8,8%

if %foldernum%=="" goto compile

set v=%~0
set cdir=%v:~0,-17%
echo cdir=%cdir%

set folder=%cdir%%foldernum%
echo folder=%folder%
echo foldernum=%foldernum%

set ffmpeg_exe=%cdir%tools\ffmpeg.exe
set metamp3_exe=%cdir%tools\metamp3.exe
echo ffmpeg_exe=%ffmpeg_exe%
echo metamp3_exe=%metamp3_exe%

for %%i in (%folder%\MP4\*_???.mp4) do (
%ffmpeg_exe% -i %%~dpi\%%~ni.mp4 -ss 00:00:11.000 -frames 1 -y %%~dpi\%%~ni.jpeg
%ffmpeg_exe% -i %%~dpi\%%~ni.mp4 -s 160x90 -ss 00:00:11.000 -frames 1 -y %%~dpi\%%~ni.jpg
)

for %%i in (%folder%\MP4-HD\*_???.mp4) do (
%ffmpeg_exe% -i %%~dpi\%%~ni.mp4 -ss 00:00:11.000 -frames 1 -y %%~dpi\%%~ni.jpeg
)

%cdir%tools\php\php.exe %cdir%tools\create_full_jpeg.php %folder%

:compile