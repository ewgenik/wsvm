echo on

set arg1=%1
set foldernum=%arg1:~-8,8%

if %foldernum%=="" goto compile

set v=%~0
set cdir=%v:~0,-25%
echo cdir=%cdir%

set folder=%cdir%%foldernum%
echo folder=%folder%
echo foldernum=%foldernum%

%cdir%tools\php\php.exe %cdir%tools\create_full_jpeg.php %folder%

start %cdir%\%foldernum%\MP4-HD\%foldernum%.jpeg

:compile