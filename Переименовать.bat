@echo on
set arg1=%1
set foldernum=%arg1:~-8,8%

set v=%~0
set cdir=%v:~0,-17%

set folder=%cdir%%foldernum%
echo folder=%folder%


if %foldernum%=="" goto compile

md %folder%\MP4
set mp4prefix=_MainConcept AVC-AAC_XBE 400p
FOR /L %%i IN (0,1,9) DO move "%folder%\%foldernum%%mp4prefix%[%%i].mp4" %folder%\MP4\%foldernum%_00%%i.mp4
FOR /L %%i IN (10,1,99) DO move "%folder%\%foldernum%%mp4prefix%[%%i].mp4" %folder%\MP4\%foldernum%_0%%i.mp4

md %folder%\MP4-HD
rem set mp4hdprefix=_MainConcept AVC-AAC_XBE 720p
set mp4hdprefix=_MainConcept AVC-AAC_XBE 50 1080p

FOR /L %%i IN (0,1,9) DO move "%folder%\%foldernum%%mp4hdprefix%[%%i].mp4" %folder%\MP4-HD\%foldernum%_00%%i.mp4
FOR /L %%i IN (10,1,99) DO move "%folder%\%foldernum%%mp4hdprefix%[%%i].mp4" %folder%\MP4-HD\%foldernum%_0%%i.mp4

md %folder%\MP3
set mp3prefix=_MP3 Audio_XBE 44100 HQ
FOR /L %%i IN (0,1,9) DO move "%folder%\%foldernum%%mp3prefix%[%%i].mp3" %folder%\MP3\%foldernum%_00%%i.mp3
FOR /L %%i IN (10,1,99) DO move "%folder%\%foldernum%%mp3prefix%[%%i].mp3" %folder%\MP3\%foldernum%_0%%i.mp3

:compile

pause
