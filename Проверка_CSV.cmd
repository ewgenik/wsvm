@echo off
set arg1=%1
set foldernum=%arg1:~-8,8%

set v=%~0
set cdir=%v:~0,-16%

set folder=%cdir%%foldernum%
echo folder=%folder%

if %foldernum%=="" goto compile

md %folder%\PNG

%cdir%tools\php\php.exe %cdir%tools\testcsv.php %folder%
pause
