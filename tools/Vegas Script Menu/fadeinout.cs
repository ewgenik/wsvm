/**
 * Sample script that performs batch renders with GUI for selecting
 * render templates.
 *
 * Revision Date: Jun. 28, 2006.
 **/
using System;
using System.IO;
using System.Text;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Forms;

using ScriptPortal.Vegas;

public class EntryPoint {
    ScriptPortal.Vegas.Vegas vegas = null;
    public void FromVegas(Vegas vegas)
    {
  	  string DirectoryName = Path.GetDirectoryName(vegas.Project.FilePath);
  	  string FileName = Path.GetFileNameWithoutExtension(vegas.Project.FilePath);
//    	string LogPath = String.Format(@"E:\ws\{0}\TEMP", FileName);
//			System.IO.Directory.CreateDirectory(LogPath);
    	LogFile logf = new LogFile(vegas, String.Format("{0}\\TEMP\\{1}.log", DirectoryName, DateTime.Now.Ticks));

      foreach (ScriptPortal.Vegas.Track track in vegas.Project.Tracks) {
      	if (track.Selected & track.IsVideo()) {
		    	logf.AddLogEntry(String.Format("Track Name: {0}", track.Name));
//    		  foreach (ScriptPortal.Vegas.TrackEvent trackevent in vegas.Project.Tracks[0].Events) {
    		  foreach (ScriptPortal.Vegas.TrackEvent trackevent in track.Events) {
			    	logf.AddLogEntry(String.Format("	Event Name: {0}", trackevent.ActiveTake.Name));
    				foreach (ScriptPortal.Vegas.Region region in vegas.Project.Regions) {
    					if ((region.End - region.Length) == trackevent.Start) {
								trackevent.FadeIn.Length = Timecode.FromMilliseconds(1000);
    					}
    					if (region.End == trackevent.End) {
								trackevent.FadeOut.Length = Timecode.FromMilliseconds(1000);
    					}
//				    	logf.AddLogEntry(String.Format("{0}_{1};;;", FileName, regionIndex.ToString("D3")));
						}
					}
      	}
      }


    	logf.Close();
    	logf.ShowLogAsDialog("sss");
    }
}
