/**
 * Sample script that performs batch renders with GUI for selecting
 * render templates.
 *
 * Revision Date: Jun. 28, 2006.
 **/
using System;
using System.IO;
using System.Text;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Forms;
using System.Runtime.InteropServices;     


using ScriptPortal.Vegas;


public class EntryPoint {
    ScriptPortal.Vegas.Vegas vegas = null;
    public void FromVegas(Vegas vegas)
    {
	string DirectoryName = Path.GetDirectoryName(vegas.Project.FilePath);
	string FileName = Path.GetFileNameWithoutExtension(vegas.Project.FilePath);

    	string CSVPath = String.Format("{0}\\TEMP", DirectoryName);
	System.IO.Directory.CreateDirectory(CSVPath);

	string CSVFileName = String.Format("{0}\\{1}.csv", CSVPath, FileName);
	Encoding win1251 = Encoding.GetEncoding(1251);
	string str = String.Format("{0};Церковь ХВЕ города Томска;Богослужение {1}.{2}.{3};xbe.tomsk.ru\r\n", FileName, FileName.Substring(6,2), FileName.Substring(4,2), FileName.Substring(0,4));
     	File.AppendAllText(CSVFileName, str, win1251);

	int regionIndex = 0;
	foreach (ScriptPortal.Vegas.Region region in vegas.Project.Regions) {
		regionIndex++;
	     	File.AppendAllText(CSVFileName, String.Format("{0}_{1};;;\r\n", FileName, regionIndex.ToString("D3")));
	}
	System.Diagnostics.Process.Start(CSVFileName);
    }
}
