/**
 * Sample script that performs batch renders with GUI for selecting
 * render templates.
 *
 * Revision Date: Jun. 28, 2006.
 **/
using System;
using System.IO;
using System.Text;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Forms;

using ScriptPortal.Vegas;

public class EntryPoint {
    ScriptPortal.Vegas.Vegas vegas = null;
    public void FromVegas(Vegas vegas)
    {
	int regionIndex = 0;
	int cersoroutofregions = 1;

	foreach (ScriptPortal.Vegas.Region region in vegas.Project.Regions) {
		if ((region.End - region.Length) > vegas.Transport.CursorPosition) {
			cersoroutofregions = 0;
			break;
		}
		regionIndex++;
	}

	if (cersoroutofregions == 1) {
		regionIndex = 0;
	}

	vegas.Transport.CursorPosition = vegas.Project.Regions[regionIndex].End - vegas.Project.Regions[regionIndex].Length;
    	vegas.Transport.Play();
    }
}
