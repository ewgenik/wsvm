/**
 * Sample script that performs batch renders with GUI for selecting
 * render templates.
 *
 * Revision Date: Jun. 28, 2006.
 **/
using System;
using System.IO;
using System.Text;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Forms;

using ScriptPortal.Vegas;

public class EntryPoint {
    ScriptPortal.Vegas.Vegas vegas = null;
    public void FromVegas(Vegas vegas)
    {

  	  vegas.Project.Tracks[0].Selected = true;
  	  string DirectoryName = Path.GetDirectoryName(vegas.Project.FilePath);
  	  string FileName = Path.GetFileNameWithoutExtension(vegas.Project.FilePath);
//  	  MessageBox.Show(FileName);
//  	  MessageBox.Show(Path.GetDirectoryName(vegas.Project.FilePath));
//  	  MessageBox.Show(Path.GetDirectoryName(vegas.Project.FilePath));
//  	  GetFileName 
/*    	Media media1 = new Media("E:\\ws\\tools\\def-720.png");
    	Media media2 = new Media("E:\\ws\\tools\\stih-720.png");
	    VideoTrack track = vegas.Project.AddVideoTrack();
	    VideoEvent videoEvent1 = track.AddVideoEvent(Timecode.FromFrames(300), Timecode.FromFrames(300));
	    Take take1 = videoEvent1.AddTake(media1.GetVideoStreamByIndex(0));
	    VideoEvent videoEvent2 = track.AddVideoEvent(Timecode.FromFrames(550), Timecode.FromFrames(300));
	    Take take2 = videoEvent2.AddTake(media2.GetVideoStreamByIndex(0));*/

//    	Media media1 = new Media("E:\\ws\\tools\\def-720.png");
//    	Media media2 = new Media("E:\\ws\\tools\\stih-720.png");
	    VideoTrack track = vegas.Project.AddVideoTrack();
/*	    VideoEvent videoEvent1 = track.AddVideoEvent(Timecode.FromFrames(300), Timecode.FromFrames(300));
	    Take take1 = videoEvent1.AddTake(new Media("E:\\ws\\tools\\def-720.png").GetVideoStreamByIndex(0));
	    VideoEvent videoEvent2 = track.AddVideoEvent(Timecode.FromFrames(550), Timecode.FromFrames(300));
	    Take take2 = videoEvent2.AddTake(new Media("E:\\ws\\tools\\stih-720.png").GetVideoStreamByIndex(0));*/
			
			int regionIndex = 0;

			foreach (ScriptPortal.Vegas.Region region in vegas.Project.Regions) {
				regionIndex++;
//				MessageBox.Show(regionIndex.ToString("D3"));
		    vegas.Transport.CursorPosition = region.Position;
//		    String.Format("{0}\\PNG\\{1}.png", DirectoryName, FileName);
		    track.AddVideoEvent(region.Position + Timecode.FromMilliseconds(5000), Timecode.FromMilliseconds(5000)).AddTake(new Media(String.Format("{0}\\PNG\\{1}.png", DirectoryName, FileName)).GetVideoStreamByIndex(0));
		    track.AddVideoEvent(region.Position + Timecode.FromMilliseconds(9000), Timecode.FromMilliseconds(5000)).AddTake(new Media(String.Format("{0}\\PNG\\{1}_{2}.png", DirectoryName, FileName, regionIndex.ToString("D3"))).GetVideoStreamByIndex(0));
			}

      foreach (ScriptPortal.Vegas.TrackEvent trackevent in vegas.Project.Tracks[0].Events) {
//				MessageBox.Show(trackevent.ActiveTake.Name);
				trackevent.FadeIn.Length = Timecode.FromMilliseconds(1000);
				trackevent.FadeOut.Length = Timecode.FromMilliseconds(1000);
//				trackevent.Length = Timecode.FromFrames(300);
			}


/*			vegas.OpenFile("E:\\ws\\tools\\def-720.png");
    	vegas.Transport.CursorPosition = Timecode.FromMilliseconds(vegas.Transport.CursorPosition.ToMilliseconds() - 100);
			vegas.OpenFile("E:\\ws\\tools\\stih-720.png");*/
/* OK
			foreach (ScriptPortal.Vegas.Region region in vegas.Project.Regions) {
		    vegas.Transport.CursorPosition = region.Position;
				vegas.OpenFile("E:\\ws\\tools\\def-720.png");
	    	vegas.Transport.CursorPosition = Timecode.FromMilliseconds(vegas.Transport.CursorPosition.ToMilliseconds() - 100);
				vegas.OpenFile("E:\\ws\\tools\\stih-720.png");
			}


      foreach (ScriptPortal.Vegas.TrackEvent trackevent in vegas.Project.Tracks[0].Events) {
//				MessageBox.Show(trackevent.ActiveTake.Name);
				trackevent.Length = Timecode.FromFrames(300);
			}
OK */

    }
}
