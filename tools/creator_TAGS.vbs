Set args = WScript.Arguments
strFolder = args.Item(0)
strPath = args.Item(1)

Set objFSO = CreateObject("Scripting.FileSystemObject")
Set objCSVFile = objFSO.OpenTextFile(strPath & "\" & strFolder & "\TEMP\" & strFolder & ".csv",1)
Set objResultFile = objFSO.CreateTextFile(strPath & "\" & strFolder & "\TEMP\" & strFolder & "_TAGS.bat",true)

strResults = "echo on" & vbCrLf 
strResults = strResults & "set metamp3_exe=" & strPath & "\tools\metamp3.exe" & vbCrLf 

Do while NOT objCSVFile.AtEndOfStream
	if Instr(objCSVFile.AtEndOfStream, Chr(34) & Chr(34)) > 0 then
		strReadLine = Replace(objCSVFile.ReadLine, Chr(34) & ";", ";")
		strReadLine = Replace(strReadLine, ";" & Chr(34), ";")
		strReadLine = Replace(strReadLine, Chr(34) & Chr(34) ,Chr(34))
	else
		strReadLine = objCSVFile.ReadLine
	End If
'	WScript.Echo strReadLine
	arrStr = split(strReadLine,";")
	If arrStr(0) <> "" And arrStr(0) <> "string not to include" Then
			if len(arrStr(0)) = 8 then
				strSite = arrStr(3)
'				������� ������ mp3
'				strResults = strResults & "%metamp3_exe% --frame TCOP:" & Chr(34) & "����� ��������, ����� �������" & Chr(34) & " --frame WXXX:" & Chr(34) & "http://" & strSite & Chr(34) & " --frame TENC:" & Chr(34) & "Vladimir Krasovsky" & Chr(34) & " --frame APIC:" & strFolder & "\FLV\" & arrStr(0) & ".jpeg --artist @" & strPath & "\" & strFolder & "\TEMP\" & arrStr(0) & "_artist.tag" & " --title @" & strPath & "\" & strFolder & "\TEMP\" & arrStr(0) & "_title.tag" & " --genre " & Chr(34) & "������������" & Chr(34) & " --2 " & strFolder & "\MP3\" & arrStr(0) & ".mp3" & vbCrLf
			else
				strResults = strResults & "%metamp3_exe% --track " & CInt(mid(arrStr(0), 10, 3)) & " --year " & mid(arrStr(0), 1, 4) & " --comment @" & strPath & "\" & strFolder & "\TEMP\" & arrStr(0)  & ".txt " & " --frame TCOP:" & Chr(34) & "����� ��������, ����� �������" & Chr(34) & " --frame WXXX:" & Chr(34) & "http://" & strSite & Chr(34) & " --frame TENC:" & Chr(34) & "Ewgeniy B. Nikonorov" & Chr(34) & " --frame APIC:" & strFolder & "\MP4-HD\" & arrStr(0) & ".jpeg --artist @" & strPath & "\" & strFolder & "\TEMP\" & arrStr(0) & "_artist.tag" & " --title @" & strPath & "\" & strFolder & "\TEMP\" & arrStr(0) & "_title.tag" & " --album @" & strPath & "\" & strFolder & "\TEMP\" & mid(arrStr(0), 1, 8) & "_title.tag" & " --genre " & Chr(34) & arrStr(3) & Chr(34) & " --2 " & strPath & "\" & strFolder & "\MP3\" & arrStr(0) & ".mp3" & vbCrLf
			end if
			
			Set objTagFile = objFSO.CreateTextFile(strPath & "\" & strFolder & "\TEMP\" & arrStr(0) & "_artist.tag",true)
			objTagFile.Write arrStr(1)
			objTagFile.Close
			
			Set objTagFile = objFSO.CreateTextFile(strPath & "\" & strFolder & "\TEMP\" & arrStr(0) & "_title.tag",true)
			objTagFile.Write arrStr(2)
			objTagFile.Close
			

	End If
Loop


Function StrConv(Text, SourceCharset, DestCharset) 
  With CreateObject("ADODB.Stream") 
    .Type = 2 
    .Mode = 3 
    .Open 
    .Charset  = DestCharset 
    .WriteText Text 
    .Position = 0 
    .Charset  = SourceCharset 
    StrConv   = .ReadText 
  End With 
End Function

strResults = StrConv(strResults, "windows-1251", "cp866")

objResultFile.Write strResults
objResultFile.Close
objCSVFile.Close
'WScript.Echo strResults