<?php 
	define('DS', DIRECTORY_SEPARATOR);

	$arg_folder = explode("\\", $argv[1]);
	print_r($arg_folder);
	$folder_num = end($arg_folder);
	print_r('folder_num='.$folder_num."\n\r");
	$folder_prefix = substr($argv[1], 0, -8);
	print_r('folder_prefix='.$folder_prefix."\n\r");
	$folder_skin = 'skins/modern.sb01';
//	$folder_skin = 'skins/ochve';
//	$folder_skin = 'skins/christmas';
	$csvfile = $folder_prefix.$folder_num.DS.'TEMP'.DS.$folder_num.'.csv';
	$csv = mb_convert_encoding(file_get_contents($csvfile), "utf-8", "windows-1251");

	foreach(preg_split("/((\r?\n)|(\r\n?))/", $csv) as $line){
		if (!strlen($line)) { break; }
		$wsitem = explode(";", $line);
		switch ($wsitem[3]) {
			case 'Проповедь':
				$wsitem[4] = $folder_skin.'/propoved.png';
			break;
			case 'Пение':
				$wsitem[4] = $folder_skin.'/penie.png';
			break;
			case 'Стихотворение':
				$wsitem[4] = $folder_skin.'/stih.png';
			break;
			default:
				$wsitem[4] = $folder_skin.'/def.png';
		}
		print_r($wsitem);

		// Создаем картинку, открываем PNG и подгоняем под 1920, 1080
		$img = imagecreatetruecolor(1920, 1080);
		imagealphablending($img, false);
		$img1 = imagecreatefrompng('tools\\'.$wsitem[4]);
		imagecopyresampled($img, $img1, 0, 0, 0, 0, 1920, 1080, imagesx($img1), imagesy($img1));
		imagesavealpha($img, true);
		$color = imagecolorallocatealpha($img, 0, 0, 0, 127);
		$poster_text_font = "C:\\Windows\\Fonts\\calibrib.ttf";
		$poster_text_color = imagecolorallocate($img, 0xbff, 0xff, 0xff);
		$url_text_color = imagecolorallocate($img, 0xbaa, 0xaa, 0xaa);
		mytext($img, mb_strtoupper($wsitem[2]), 40, 386, 896, $poster_text_color);
		mytext($img, $wsitem[1], 40, 1660, 964, $poster_text_color, 'right');
		mytext($img, 'xbe.tomsk.ru', 26, 1888, 1040, $poster_text_color, 'right', 0);
		imagepng($img, $argv[1].'\\PNG\\'.$wsitem[0].'.png');
	}

function mytext(
    $image,
    $text,
    $size,
    $x,
    $y,
    $color,
    $align = 'left',
    $bg = 1,
    $angle = 0,
    $font_filename = "C:\\Windows\\Fonts\\calibrib.ttf"
) {

	$max_text_width = 1330;

	$dimensions = imagettfbbox($size, $angle, $font_filename, $text);
	$text_width = abs($dimensions[4] - $dimensions[0]);
	while($text_width > $max_text_width) {
//		echo $text_width."\n\r";
    $size = $size * 0.99;
		$dimensions = imagettfbbox($size, $angle, $font_filename, $text);
		$text_width = abs($dimensions[4] - $dimensions[0]);
	}
	
	if ($align == 'right') {
		$x = $x - $text_width;
	}
	
//	$text = $text." [$text_width]";
	
	$bordercolor = imagecolorallocate($image, 0xb0f, 0x6c, 0xa9);
	if ($bg) {
		imagefttext($image, $size, $angle, $x-2, $y, $bordercolor, $font_filename, $text);
		imagefttext($image, $size, $angle, $x+2, $y, $bordercolor, $font_filename, $text);
		imagefttext($image, $size, $angle, $x, $y-2, $bordercolor, $font_filename, $text);
		imagefttext($image, $size, $angle, $x, $y+2, $bordercolor, $font_filename, $text);
	}
	imagefttext($image, $size, $angle, $x, $y, $color, $font_filename, $text);
}

?>