@echo on
set arg1=%1
set foldernum=%arg1:~-8,8%

set v=%~0
set cdir=%v:~0,-17%

set folder=%cdir%%foldernum%
echo folder=%folder%

if %foldernum%=="" goto compile


md %folder%\PNG

%cdir%tools\php\php.exe %cdir%tools\create_png.php %folder%
rem %cdir%tools\php\php.exe %cdir%tools\create_full_png.php %folder%
rem pause

:compile
